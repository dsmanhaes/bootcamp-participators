import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';

@Injectable()
export class EventsService {
  private includes = {
    creator: true,
    categories: true,
    participators: true
  };

  constructor(private prisma: PrismaService) {}

  async create(createEventDto: CreateEventDto) {
    const {categories, participators, ...createEvent} = createEventDto;
    const event = await this.prisma.event.create({
      data: {
        ...createEvent,
        categories: { connect: categories },
        participators: { connect: participators }
      },
      include: this.includes
    });
    delete event.creator.password;
    event.participators.forEach((user) => delete user.password);
    return event;
  }

  async findAll() {
    const events = await this.prisma.event.findMany({ include: this.includes });
    events.forEach((event) => {
      delete event.creator.password;
      event.participators.forEach((user) => delete user.password);
    });
    return events;
  }

  async findOne(id: string) {
    const event = await this.prisma.event.findUnique({ where: { id }, include: this.includes });
    delete event.creator.password;
    event.participators.forEach((user) => delete user.password);
    return event;
  }

  async update(id: string, updateEventDto: UpdateEventDto) {
    const {categories, participators, ...updateEvent} = updateEventDto;
    const event = await this.prisma.event.update({
      where: { id },
      data: {
        ...updateEvent,
        categories: { connect: categories },
        participators: { connect: participators }
      },
      include: this.includes
    });
    delete event.creator.password;
    event.participators.forEach((user) => delete user.password);
    return event;
  }

  remove(id: string) {
    return this.prisma.event.delete({ where: { id }, include: this.includes });
  }
}
