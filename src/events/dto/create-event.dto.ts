import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsArray, IsNotEmpty, IsObject, IsOptional, IsString, IsUrl, MaxLength, MinLength } from "class-validator";
import { CategoryEntity } from "../../categories/entities/category.entity";
import { UserEntity } from "../../users/entities/user.entity";

export class CreateEventDto {
  @IsString()
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(127)
  @ApiProperty()
  name: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(500)
  @ApiProperty()
  description: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  place: string;

  @IsUrl()
  @IsOptional()
  @ApiPropertyOptional()
  photo: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  creator_id: string;

  @IsArray()
  @IsOptional()
  @ApiPropertyOptional()
  categories: CategoryEntity[];

  @IsArray()
  @IsOptional()
  @ApiPropertyOptional()
  participators: UserEntity[];
}
