import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsOptional, IsString, IsStrongPassword, IsUrl, Max, MaxLength, MinLength } from "class-validator";

export class CreateUserDto {
  @IsEmail()
  @IsNotEmpty()
  @ApiProperty()
  email: string;

  @IsStrongPassword()
  @IsNotEmpty()
  @ApiProperty()
  password: string;

  @IsString()
  @IsOptional()
  @MaxLength(15)
  @ApiPropertyOptional()
  username?: string;

  @IsString()
  @IsOptional()
  @MinLength(3)
  @MaxLength(255)
  @ApiPropertyOptional()
  name?: string;

  @IsUrl()
  @IsOptional()
  @ApiPropertyOptional()
  photo?: string;
}
