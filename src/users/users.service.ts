import { BadRequestException, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Bcrypt } from '../auth/bcrypt';

@Injectable()
export class UsersService {
  private includes = {
    created: true,
    participating: true
  };

  constructor(
    private prisma: PrismaService,
    private bcrypt: Bcrypt
  ) {}

  async create(createUserDto: CreateUserDto) {
    createUserDto.password = this.bcrypt.encryptPassword(createUserDto.password);
    const user = await this.prisma.user.create({ data: createUserDto, include: this.includes });
    delete user.password;
    return user;
  }

  async findAll() {
    const users = await this.prisma.user.findMany({ include: this.includes });
    users.forEach((user) => delete user.password);
    return users;
  }

  async findById(id: string){
    const user = await this.prisma.user.findUnique({ where: { id }, include: this.includes });
    delete user.password;
    return user;
  }

  async findByEmail(email: string) {
    const user = await this.prisma.user.findUnique({ where: { email }, include: this.includes });
    delete user.password;
    return user;
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    const user = await this.prisma.user.update({
      where: { id },
      data: updateUserDto,
      include: this.includes
    });
    delete user.password;
    return user;
  }

  remove(id: string) {
    if (!this.findById(id)) throw new BadRequestException('Id not found.')
    return this.prisma.user.delete({ where: { id }, include: this.includes });
  }
}
