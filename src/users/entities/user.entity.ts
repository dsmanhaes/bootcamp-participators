import { ApiHideProperty, ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { User } from "@prisma/client";

export class UserEntity implements User {
  @ApiProperty()
  id: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  password: string;

  @ApiPropertyOptional()
  username: string | null;

  @ApiPropertyOptional()
  name: string | null;

  @ApiPropertyOptional()
  photo: string | null;

  @ApiHideProperty()
  created_at: Date;

  @ApiHideProperty()
  updated_at: Date;
}
