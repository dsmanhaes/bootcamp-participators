import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { PrismaModule } from '../prisma/prisma.module';
import { Bcrypt } from '../auth/bcrypt';

@Module({
  controllers: [UsersController],
  providers: [UsersService, Bcrypt],
  imports: [PrismaModule],
  exports: [UsersService]
})
export class UsersModule {}
