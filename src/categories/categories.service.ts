import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';

@Injectable()
export class CategoriesService {
  private includes = {
    events: true
  };

  constructor(private prisma: PrismaService) {}

  create(createCategoryDto: CreateCategoryDto) {
    return this.prisma.category.create({ data: createCategoryDto, include: this.includes });
  }

  findAll() {
    return this.prisma.category.findMany({ include: this.includes });
  }

  findOne(id: string) {
    return this.prisma.category.findUnique({ where: { id }, include: this.includes });
  }

  update(id: string, updateCategoryDto: UpdateCategoryDto) {
    return this.prisma.category.update({
      where: { id },
      data: updateCategoryDto,
      include: this.includes
    });
  }

  remove(id: string) {
    return this.prisma.category.delete({ where: { id }, include: this.includes });
  }
}
