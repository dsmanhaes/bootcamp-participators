import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString, IsUrl, MaxLength, MinLength } from "class-validator";

export class CreateCategoryDto {
  @IsString()
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(127)
  @ApiProperty()
  name: string;

  @IsUrl()
  @IsOptional()
  @ApiPropertyOptional()
  photo?: string;
}
