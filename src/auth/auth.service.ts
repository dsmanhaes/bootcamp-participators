import { Injectable, NotFoundException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { UserEntity } from "src/users/entities/user.entity";
import { UsersService } from "../users/users.service";
import { Bcrypt } from "./bcrypt";

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
    private bcrypt: Bcrypt
  ) {}

  async validateUser (username: string, password: string) {
    const user = await this.userService.findByEmail(username);
    if (!user) throw new NotFoundException('User not found');
    const match = this.bcrypt.comparePassword(password, user.password);
    return (user && match) ? user : null;
  }

  login (user: UserEntity) {
    const payload = { username: user.username };
    return {
      id: user.id,
      username: user.email,
      token: `Bearer ${this.jwtService.sign(payload)}`
    };
  }
}