import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty } from "class-validator";

export class LoginUserDto {
  @IsEmail()
  @IsNotEmpty()
  @ApiProperty({example: 'email'})
  username: string;

  @IsNotEmpty()
  @ApiProperty()
  password: string;
}
