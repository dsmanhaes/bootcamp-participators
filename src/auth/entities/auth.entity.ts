import { ApiProperty } from "@nestjs/swagger";

export class AuthEntity {
  @ApiProperty()
  id: string;

  @ApiProperty()
  username: string;

  @ApiProperty()
  token: string;
}
