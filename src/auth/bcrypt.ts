import * as bcrypt from 'bcrypt';

export class Bcrypt {
  encryptPassword(password: string) {
    return bcrypt.hashSync(password, 10);
  }

  comparePassword(password: string, hash: string) {
    return bcrypt.compareSync(password, hash);
  }
}
