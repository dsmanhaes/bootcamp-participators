import { Body, Controller, HttpCode, HttpStatus, Post, Req, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { ApiOkResponse, ApiTags } from "@nestjs/swagger";
import { Request } from "express";
import { UserEntity } from "src/users/entities/user.entity";
import { AuthService } from "./auth.service";
import { LoginUserDto } from "./dto/login-user.dto";
import { AuthEntity } from "./entities/auth.entity";

@ApiTags('Users')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @ApiOkResponse({ type: AuthEntity })
  @HttpCode(HttpStatus.OK)
  @Post()
  login(@Body() user: LoginUserDto, @Req() req: Request) {
    return this.authService.login(req.user as UserEntity);
  }
}
